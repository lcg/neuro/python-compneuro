lcg-neuro-compneuro – Computational neuroscience library for Python
===================================================================

|image0|
|image1|

|image2|
|image3|

|image4|
|image5|
|image6|

-  `Documentation`_
-  `Issue tracker`_
-  `Repository contents`_
-  `History of changes`_
-  `Contribution/development guide`_
-  `Copy of MIT License`_

--------------

Installation
------------

.. code:: bash

    pip install lcg-neuro-compneuro

--------------

-  Powered by `GitLab CI`_
-  Created by `Pedro Asad <pasad@lcg.ufrj.br>`_ using `cookiecutter`_ and `gl:pedroasad.com/templates/python/python/app@1.1.0`_

.. _Documentation: https://lcg.gitlab.io/neuro/python-compneuro
.. _Issue tracker: https://gitlab.com/lcg/neuro/python-compneuro/issues
.. _Repository contents: MANIFEST.md
.. _History of changes: CHANGELOG.md
.. _Contribution/development guide: CONTRIBUTING.md
.. _Copy of MIT License: LICENSE.txt
.. _GitLab CI: https://docs.gitlab.com/ee/ci
.. _Pedro Asad <pasad@lcg.ufrj.br>: mailto:pasad@lcg.ufrj.br
.. _cookiecutter: http://cookiecutter.readthedocs.io/
.. _`gl:pedroasad.com/templates/python/python/app@1.1.0`: https://gitlab.com/pedroasad.com/templates/python/python-app/tags/1.1.0

.. |image0| image:: https://img.shields.io/badge/Python-%E2%89%A53.6-blue.svg
   :target: https://docs.python.org/3.6
.. |image1| image:: https://img.shields.io/badge/version-0.1.0%20None-orange.svg
   :target: https://test.pypi.org/project/lcg-neuro-compneuro/0.1.0/
.. |image2| image:: https://img.shields.io/badge/license-MIT-blue.svg
   :target: https://opensource.org/licenses/MIT
.. |image3| image:: https://img.shields.io/badge/code%20style-Black-black.svg
   :target: https://pypi.org/project/black/
.. |image4| image:: https://gitlab.com/lcg/neuro/python-compneuro/badges/master/pipeline.svg
   :target: https://gitlab.com/lcg/neuro/python-compneuro
.. |image5| image:: https://img.shields.io/badge/security-Check%20here!-yellow.svg
   :target: https://gitlab.com/lcg/neuro/python-compneuro/security
.. |image6| image:: https://codecov.io/gl/lcg:neuro/python-compneuro/branch/master/graph/badge.svg
   :target: https://codecov.io/gl/lcg:neuro/python-compneuro
