Manifest
========

-  `.bumpversion.cfg`_ -- Configuration file for the `bumpversion`_ version-tagging package.
-  `.coveragerc`_ -- Configuration file for the `Coverage`_ reporting tool.
-  `.gitignore`_ -- List of files and directories paths/patterns `ignored by Git`_.
-  `.gitlab-ci.yml`_ -- Continuous integration/deploy configuration (`GitLab CI`_), configured for:
-  Running a `pytest`_-based test suite and reporting results with `Codecov`_ at https://codecov.io/gl/lcg:neuro/python-compneuro,
-  Building the `Sphinx`_-based documentation and deploying it via [Gitlab Pages] to https://lcg.gitlab.io/neuro/python-compneuro, and
-  Uploading successfully built, tagged distributions to `PyPI`_ (defaults to https://pypi.org/project/lcg-neuro-compneuro).
-  `.pre-commit-config.yaml`_ -- Configuration file for the `pre-commit`_ package, which aids in applying useful `Git Hooks`_ in team workflows. Includes:
-  Automatic code styling with `Black`_.
-  `CHANGELOG.md`_ -- history of changes, which follows the `Keep a Changelog`_ standard.
-  `LICENSE.txt`_ -- Copy of the `MIT License`_.
-  `MANIFEST.md`_ this very file.
-  `README.md`_ -- repository front-page.
-  `docs/`_ -- `Sphinx`_-based documentation setup, which includes:
-  `conf.py`_ -- `Sphinx`_ configuration file,
-  `docs/index.rst`_ -- documentation master file, and
-  `Makefile`_ for building the docs easily.
-  `poetry.lock`_ -- `Poetry`_'s resolved dependency file. Whereas `pyproject.toml`_ specifies `as abstract as possible`_ dependencies, this file
-  `pyproject.toml`_ -- `PEP-517`_-compliant packaging metadata, configured with the `Poetry`_ system. Includes, among other information: package qualifiers, version, author, and all of its dependencies. This file replaces the classic `setup.py`_ file found in *classical* Python packaging.
-  `src/compneuro`_ -- Base directory of the example Python package distributed by this repository.
-  `tests`_ -- `pytest`_-powered test-suite.

.. _.bumpversion.cfg: https://gitlab.com/lcg/neuro/python-compneuro/blob/master/.bumpversion.cfg
.. _bumpversion: https://github.com/peritus/bumpversion
.. _.coveragerc: https://gitlab.com/lcg/neuro/python-compneuro/blob/master/.coveragerc
.. _Coverage: https://coverage.readthedocs.io
.. _.gitignore: https://gitlab.com/lcg/neuro/python-compneuro/blob/master/.gitignore
.. _ignored by Git: https://git-scm.com/docs/gitignore
.. _.gitlab-ci.yml: https://gitlab.com/lcg/neuro/python-compneuro/blob/master/.gitlab-ci.yml
.. _GitLab CI: https://docs.gitlab.com/ee/ci
.. _pytest: https://pytest.org/
.. _Codecov: https://codecov.io
.. _Sphinx: https://www.sphinx-doc.org/en/master/index.html
.. _PyPI: https://pypi.org
.. _.pre-commit-config.yaml: https://gitlab.com/lcg/neuro/python-compneuro/blob/master/.pre-commit-config.yaml
.. _pre-commit: https://pre-commit.com/
.. _Git Hooks: https://git-scm.com/book/en/v2/Customizing-Git-Git-Hooks
.. _Black: https://pypi.org/project/black/
.. _CHANGELOG.md: https://gitlab.com/lcg/neuro/python-compneuro/blob/master/CHANGELOG.md
.. _Keep a Changelog: https://keepachangelog.com/en/1.0.0/
.. _LICENSE.txt: https://gitlab.com/lcg/neuro/python-compneuro/blob/master/LICENSE.txt
.. _MIT License: https://opensource.org/licenses/MIT
.. _MANIFEST.md: https://gitlab.com/lcg/neuro/python-compneuro/blob/master/MANIFEST.md
.. _README.md: https://gitlab.com/lcg/neuro/python-compneuro/blob/master/README.md
.. _docs/: https://gitlab.com/lcg/neuro/python-compneuro/blob/master/docs
.. _conf.py: https://gitlab.com/lcg/neuro/python-compneuro/blob/master/docs/conf.py
.. _docs/index.rst: https://gitlab.com/lcg/neuro/python-compneuro/blob/master/docs/index.rst
.. _Makefile: https://gitlab.com/lcg/neuro/python-compneuro/blob/master/docs/Makefile
.. _poetry.lock: https://gitlab.com/lcg/neuro/python-compneuro/blob/master/poetry.lock
.. _Poetry: https://github.com/sdispater/poetry
.. _pyproject.toml: https://gitlab.com/lcg/neuro/python-compneuro/blob/master/pyproject.toml
.. _as abstract as possible: https://caremad.io/posts/2013/07/setup-vs-requirement/
.. _PEP-517: https://www.python.org/dev/peps/pep-0517/
.. _setup.py: https://docs.python.org/3.6/distutils/setupscript.html
.. _src/compneuro: https://gitlab.com/lcg/neuro/python-compneuro/blob/master/compneuro
.. _tests: https://gitlab.com/lcg/neuro/python-compneuro/blob/master/tests
