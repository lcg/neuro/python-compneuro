:mod:`compneuro.simulate` -- Random spike train generation
==========================================================

.. automodule:: compneuro.simulate
    :members:
    :show-inheritance:
    :undoc-members:
