:mod:`compneuro.kernels.factory.rate` -- Convolution kernels generators parametrized by sampling rate
=====================================================================================================

.. automodule:: compneuro.kernels.factory.rate
    :members:
    :show-inheritance:
    :undoc-members:
