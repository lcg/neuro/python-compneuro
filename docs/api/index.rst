..
   vim: et: sw=3: ts=3: tw=120:
   vim: filetype=rst:
   vim: spell: spelllang=en:

=============
API reference
=============

.. toctree::
   :maxdepth: 2
   :glob:

   *

Bibliography
============

.. bibliography:: index.bib

