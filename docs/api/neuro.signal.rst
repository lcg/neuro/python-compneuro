:mod:`compneuro.signal` -- Spike train and firing rate analysis
===============================================================

.. automodule:: compneuro.signal
    :members:
    :show-inheritance:
    :undoc-members:
