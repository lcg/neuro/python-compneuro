:mod:`compneuro.testing` -- Unit testing support
================================================

.. automodule:: compneuro.testing
    :members:
    :show-inheritance:
    :undoc-members:
