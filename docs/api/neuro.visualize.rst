:mod:`compneuro.visualize` -- Plotting
======================================

.. automodule:: compneuro.visualize
    :members:
    :show-inheritance:
    :undoc-members:
