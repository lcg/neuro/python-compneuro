:mod:`compneuro.kernels` -- Convolution kernels
===============================================

.. automodule:: compneuro.kernels
    :members:
    :show-inheritance:
    :undoc-members:
