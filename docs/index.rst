=====================================================================
lcg-neuro-compneuro --- Computational neuroscience library for Python
=====================================================================

.. toctree::
   :maxdepth: 2

   Get started <README>
   api/index
   CHANGELOG
   CONTRIBUTING
   future
   MANIFEST


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

