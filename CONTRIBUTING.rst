Contribution guide
==================

.. contents:: Contents:
    :local:

Development setup
-----------------

**Requirements:**

-  Python ≥ 3.6
-  `Poetry`_ ≥ 0.12

1. Create a `virtualenv`_ and install dependencies. Two of the most common options are:

   -  Letting `Poetry`_ create the virtual environment and install dependencies for you (the default if you have just installed Poetry):

   .. code:: bash

      poetry install  # This will create a virtual environment in a new subdirectory                    # of $(poetry config settings.virtualenvs.path)

   An interesting variation is telling Poetry to create the environment inside the project's root folder by running

   .. code:: bash

      poetry config settings.virtualenvs.in-project true

   before the ``install`` command.

   -  If you have chosen to turn off automatic environment creation using

   .. code:: bash

      poetry config settings.virtualenvs.create false

   then the following steps should suffice:

   .. code:: bash

      virtualenv -p $(which python3.6) .venv   source venv/bin/activate.sh   poetry install

   In this case, keep in mind that you need to activate the environment using the ``source`` command before issuing any Poetry commands.
   You may choose a name for your virtual environment other than ``.venv``, but this one is already on `.gitignore`_.

2. **(Optional)** Set up `pre-commit`_. It is already included as *dev*-dependency in `pyproject.toml`_ and a `.pre-commit-config.yaml`_ file is provided.
   Hence, you just need:

.. code:: bash

    pre-commit install

Building the documentation
--------------------------

Provided that the virtual environment is active, and dependencies have been installed, it is as simple as

.. code:: bash

    make html

and the output will be placed under ``docs/_build/html``.

The `CI/CD`_ pipeline, through `GitLab Pages`_, will automatically deploy the HTML tree to https://lcg.gitlab.io/neuro/python-compneuro.
If you clone the repository and do not make further changes, documentation built after ``git push`` will most likely end up in https://organization-name.gitlab.io/group-name-if-any/repository-name.

Running the tests
-----------------

Provided that the virtual environment is active, and dependencies have been installed, it is as simple as

.. code:: bash

    pytest

This already produces a code coverage report for the `compneuro`_ package, stores it in a ``.coverage`` file and prints it.

Publishing on PyPI
------------------

If you fork this repository, set up CI variables for ``PYPI_USERNAME`` and ``PYPI_PASSWORD``, or hard-code them (not recommended, for security reasons) directly in the `.gitlab-ci.yml`_
script.
This repository is configured for publishing source distributions to `PyPI`_ on successful tag pushes.

.. _Poetry: https://github.com/sdispater/poetry
.. _virtualenv: https://virtualenv.pypa.io/en/latest/
.. _.gitignore: https://gitlab.com/lcg/neuro/python-compneuro/blob/master/.gitignore
.. _pre-commit: https://pre-commit.com/
.. _pyproject.toml: https://gitlab.com/lcg/neuro/python-compneuro/blob/master/pyproject.toml
.. _.pre-commit-config.yaml: https://gitlab.com/lcg/neuro/python-compneuro/blob/master/.pre-commit-config.yaml
.. _CI/CD: https://docs.gitlab.com/ee/ci
.. _GitLab Pages: https://docs.gitlab.com/ee/user/project/pages/
.. _compneuro: https://gitlab.com/lcg/neuro/python-compneuro/blob/master/src/compneuro
.. _.gitlab-ci.yml: https://gitlab.com/lcg/neuro/python-compneuro/blob/master/.gitlab-ci.yml
.. _PyPI: https://pypi.org
