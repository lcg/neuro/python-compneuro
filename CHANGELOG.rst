Changelog
=========

All notable changes to this project will be documented in this file.
The format is based on `Keep a Changelog`_ and this project adheres to `Semantic Versioning`_.

`Unreleased`_
-------------

`0.1.0`_ - 2019-07-19
---------------------

Added
~~~~~

-  `.bumpversion.cfg`_ — Configuration file for the `bumpversion`_ version-tagging package.
-  `.coveragerc`_ — Configuration file for the `Coverage`_ reporting tool.
-  `.gitignore`_ — List of files and directories paths/patterns `ignored by Git`_.
-  `.gitlab-ci.yml`_ — Continuous integration/deploy configuration (`GitLab CI`_), configured for:
-  Running a `pytest`_-based test suite and reporting results with `Codecov`_ at https://codecov.io/gl/lcg:neuro/python-compneuro,
-  Building the `Sphinx`_-based documentation and deploying it via `Gitlab Pages`_ to https://lcg.gitlab.io/neuro/python-compneuro, and
-  Uploading successfully built, tagged distributions to `PyPI`_ (defaults to https://pypi.org/project/lcg-neuro-compneuro).
-  `.pre-commit-config.yaml`_ — Configuration file for the `pre-commit`_ package, which aids in applying useful `Git Hooks`_ in team workflows. Includes:
-  Automatic code styling with `Black`_.
-  `CHANGELOG.md`_ — this very history file, which follows the `Keep a Changelog`_ standard.
-  `LICENSE.txt`_ — Copy of the `MIT License`_.
-  `MANIFEST.md`_ Repository's manifest.
-  `README.md`_ — repository front-page.
-  `docs/`_ — `Sphinx`_-based documentation setup, which includes:
-  `conf.py`_ — `Sphinx`_ configuration file,
-  `docs/index.rst`_ — documentation master file, and
-  `Makefile`_ for building the docs easily.
-  `poetry.lock`_ — `Poetry`_'s resolved dependency file. Whereas `pyproject.toml`_ specifies `*as abstract as possible*`_ dependencies, this file
-  `pyproject.toml`_ — `PEP-517`_-compliant packaging metadata, configured with the `Poetry`_ system. Includes, among other information: package qualifiers, version, author, and all of its dependencies. This file replaces the classic `setup.py`_ file found in *classical* Python packaging.
-  `src/compneuro`_ — Base directory of the example Python package distributed by this repository.
-  `tests`_ — `pytest`_-powered test-suite.

.. _Keep a Changelog: https://keepachangelog.com/en/1.0.0/
.. _Semantic Versioning: https://semver.org/spec/v2.0.0.html
.. _Unreleased: https://gitlab.com/lcg/neuro/python-compneuro/compare?from=release&to=master
.. _0.1.0: https://gitlab.com/lcg/neuro/python-compneuro/tags/0.1.0
.. _.bumpversion.cfg: https://gitlab.com/lcg/neuro/python-compneuro/blob/0.1.0/.bumpversion.cfg
.. _bumpversion: https://github.com/peritus/bumpversion
.. _.coveragerc: https://gitlab.com/lcg/neuro/python-compneuro/blob/0.1.0/.coveragerc
.. _Coverage: https://coverage.readthedocs.io
.. _.gitignore: https://gitlab.com/lcg/neuro/python-compneuro/blob/0.1.0/.gitignore
.. _ignored by Git: https://git-scm.com/docs/gitignore
.. _.gitlab-ci.yml: https://gitlab.com/lcg/neuro/python-compneuro/blob/0.1.0/.gitlab-ci.yml
.. _GitLab CI: https://docs.gitlab.com/ee/ci
.. _pytest: https://pytest.org/
.. _Codecov: https://codecov.io
.. _Sphinx: https://www.sphinx-doc.org/en/master/index.html
.. _Gitlab Pages: https://docs.gitlab.com/ee/user/project/pages/
.. _PyPI: https://pypi.org
.. _.pre-commit-config.yaml: https://gitlab.com/lcg/neuro/python-compneuro/blob/0.1.0/.pre-commit-config.yaml
.. _pre-commit: https://pre-commit.com/
.. _Git Hooks: https://git-scm.com/book/en/v2/Customizing-Git-Git-Hooks
.. _Black: https://pypi.org/project/black/
.. _CHANGELOG.md: https://gitlab.com/lcg/neuro/python-compneuro/blob/0.1.0/CHANGELOG.md
.. _LICENSE.txt: https://gitlab.com/lcg/neuro/python-compneuro/blob/0.1.0/LICENSE.txt
.. _MIT License: https://opensource.org/licenses/MIT
.. _MANIFEST.md: https://gitlab.com/lcg/neuro/python-compneuro/blob/0.1.0/MANIFEST.md
.. _README.md: https://gitlab.com/lcg/neuro/python-compneuro/blob/0.1.0/README.md
.. _docs/: https://gitlab.com/lcg/neuro/python-compneuro/blob/0.1.0/docs
.. _conf.py: https://gitlab.com/lcg/neuro/python-compneuro/blob/0.1.0/docs/conf.py
.. _docs/index.rst: https://gitlab.com/lcg/neuro/python-compneuro/blob/0.1.0/docs/index.rst
.. _Makefile: https://gitlab.com/lcg/neuro/python-compneuro/blob/0.1.0/docs/Makefile
.. _poetry.lock: https://gitlab.com/lcg/neuro/python-compneuro/blob/0.1.0/poetry.lock
.. _Poetry: https://github.com/sdispater/poetry
.. _pyproject.toml: https://gitlab.com/lcg/neuro/python-compneuro/blob/0.1.0/pyproject.toml
.. _*as abstract as possible*: https://caremad.io/posts/2013/07/setup-vs-requirement/
.. _PEP-517: https://www.python.org/dev/peps/pep-0517/
.. _setup.py: https://docs.python.org/3.6/distutils/setupscript.html
.. _src/compneuro: https://gitlab.com/lcg/neuro/python-compneuro/blob/0.1.0/compneuro
.. _tests: https://gitlab.com/lcg/neuro/python-compneuro/blob/0.1.0/tests
